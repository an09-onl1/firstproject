package com.example.myapplication

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlin.math.*

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        println(indZ(5.0, 10.0, 1.0))
        println(task3(5.0, 5.0))
        println(task6(1, -2))


    }
}

fun indZ(a: Double, b: Double, c: Double) = ((a - 3) * b / 2) + c

fun task3(x: Double, y: Double): Double {
    return (sin(x) + cos(y)) / (cos(x) - sin(y)) * tan(x * y)
}

fun task6(x: Int, y: Int): Boolean {
    when {
        x > 2 || y > 4 -> {
            return false
        }
        x < -2 || y > 4 -> {
            return false
        }
        x < -4 || y < -3 -> {
            return false
        }
        x > 4 || y < -3 -> {
            return false
        }
        else -> {
            return true
        }

    }
}

